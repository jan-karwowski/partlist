cmake_minimum_required (VERSION 3.9.4)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project (partlist)

include(CheckIPOSupported)

check_ipo_supported(RESULT iposupported OUTPUT error)

SET( Boost_USE_STATIC_LIBS FALSE )
SET( Boost_USE_MULTITHREADED FALSE )
FIND_PACKAGE( Boost COMPONENTS program_options REQUIRED)

set(SOURCE_FILES partlist.cpp)

add_executable(partlist ${SOURCE_FILES})
target_compile_options(partlist PRIVATE -Wall -Wextra -pedantic)

target_link_libraries(partlist -lstdc++fs ${Boost_PROGRAM_OPTIONS_LIBRARY})


if( iposupported )
    message(STATUS "IPO / LTO enabled")
    set_property(TARGET partlist PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
else()
    message(STATUS "IPO / LTO not supported: <${error}>")
endif()

install(TARGETS partlist
  DESTINATION bin)
install(FILES  utils/dmenu_udevil_mount.sh
  DESTINATION bin)
