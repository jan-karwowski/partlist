#!/bin/sh

${PARTLIST:=partlist}

success_timeout=3000

DMENU_PROGRAM="dmenu"
if [ -n "$WAYLAND_DISPLAY" ]; then
    DMENU_PROGRAM="tofi --prompt-text="
fi


try_mount () {
    notify_id=$(notify-send -p "Mounting $1" "in progress")
    udevil mount $2
    res=$?
    if [ $res -eq 0 ]; then
	notify-send -r ${notify_id} -t ${success_timeout} "Mounting $1" succeded
    else
	notify-send -r ${notify_id} -u critical  "Mounting $1" failed
    fi
}

try_umount () {
    notify_id=$(notify-send -p "Umount $1" "in progress" )
    udevil umount $2
    res=$?
    if [ $res -eq 0 ]; then
	notify-send -r ${notify_id} -t ${success_timeout} "Umount $1" succeded
    else
	notify-send -r ${notify_id} -u critical  "Umount $1" failed
    fi
}


if [ "$1" = umount ] ; then
    $PARTLIST -m | ${DMENU_PROGRAM} | while read line; do
	try_umount "$line" $(echo $line|cut -d: -f2)
    done
else
    $PARTLIST -n | ${DMENU_PROGRAM} | while read line; do
	try_mount "$line" $(echo $line|cut -d: -f2)
    done
fi
