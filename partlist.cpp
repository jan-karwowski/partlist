#include <iostream>
#include <string>
#include <vector>
#include <filesystem>
#include <fstream>
#include <algorithm>
#include <map>
#include <utility>
#include <iterator>
#include <set>
#include <sstream>
#include <boost/program_options.hpp>

namespace fs = std::filesystem;
namespace po = boost::program_options;


namespace partlist {

  int get_sysfs_num(const fs::path& path) {
    std::ifstream is;
    is.open(path);
    int val;
    is >> val;
    is.close();
    return val;
  }

  bool is_blockdev_accessible(const std::string& dev) {
    auto p = fs::path("/dev")/dev;
    bool success;
    std::ifstream is;
    is.open(p);
    success=!is.fail();
    return success;
  }

  std::vector<std::filesystem::path> list_removables() {
    std::vector<fs::path> output;
    std::vector<fs::path> queue;
    for (auto &p : std::filesystem::directory_iterator("/sys/block")) {
      auto removable = p.path()/"removable";
      int rem = get_sysfs_num(removable);
      if(rem == 1 || is_blockdev_accessible(p.path().filename().string())) {
	queue.push_back(p.path());
      }
    }

    while(!queue.empty()) {
      auto p = queue.back();
      queue.pop_back();
      auto partition = p/"partition";
      bool is_partition = fs::exists(partition) && (get_sysfs_num(partition) == 1);
      if(is_partition) {
	output.push_back(fs::path("/dev")/p.filename());
      } else {
	std::vector<fs::path> children;
	for(auto &d: fs::directory_iterator(p)) {
	  auto pp = p.filename().string();
	  auto dd = d.path().filename().string();
	  if(dd.compare(0, pp.size(), pp) == 0) {
	    children.push_back(d.path());
	  }
	}
	if(children.empty()) {
	  if(get_sysfs_num(p/"size") > 0) {
	    output.push_back(fs::path("/dev")/p.filename());
	  }
	} else {
	  std::copy(children.begin(), children.end(), std::back_inserter(queue));
	}
      }
    }
    
    return output;
  }

  std::map<fs::path, std::string> get_labels() {
    std::map<fs::path, std::string> labels;
    for(auto &l: fs::directory_iterator("/dev/disk/by-label")) {
      labels.insert(std::make_pair(fs::canonical(l.path()) ,l.path().filename().string()));
    }
    
    return labels;
  }

  /**
     dev -> mountpoint
   */
  std::map<fs::path, fs::path> get_mounted() {
    std::map<fs::path, fs::path> ret;
    std::ifstream mtab;
    mtab.open("/etc/mtab");
    std::string line;
    while(std::getline(mtab, line)){
      std::istringstream ls(line);
      fs::path dev, mountpoint;
      ls >> dev >> mountpoint;
      ret.insert(std::make_pair(dev, mountpoint));
    }
    return ret;
  }
} // namespace partlist

int main(int argc, char** argv) {
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "print usage")
    ("mounted-only,m", "print only mounted devices")
    ("not-mounted-only,n", "print only not mounted devices");
  po::variables_map vm;
  
  po::store(po::parse_command_line(argc, argv, desc), vm);

  bool only_mounted = vm.count("mounted-only"), only_not_mounted = vm.count("not-mounted-only");
  
  if(vm.count("help")) {
    std::cout << desc << std::endl;
  } else if(only_mounted && only_not_mounted) {
    std::cerr << "mounted and not-mounted options cannot be passed at once" << std::endl;
  } else {
    auto labels = partlist::get_labels();
    auto mountpoints = partlist::get_mounted();
    for(auto &r: partlist::list_removables()) {
      auto it = labels.find(r);
      auto lb = it != labels.end() ? it->second : r.filename().string();
      auto it2 = mountpoints.find(r);
      auto mp = it2 != mountpoints.end() ? it2->second : "";
      if((it2 == mountpoints.end() && only_mounted) || (it2 != mountpoints.end() && only_not_mounted)) {
      } else {
	std::cout <<  lb << ":" << r.string() << ":" << mp.string() << std::endl;
      }
    }
  }
}
